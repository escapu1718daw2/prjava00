/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package prjava00;

import java.net.InetAddress;
import java.net.UnknownHostException;



/**
 *
 * @author Sandra
 */
public class Prjava00 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        System.out.println("Hola món");
        System.out.println("Versió 0.1 del projecte prjava00");
        try{
            InetAddress addr = InetAddress.getLocalHost();
            String ipAddr = addr.getHostAddress();
            String hostName = addr.getHostName();
            System.out.println("Hostname = " + hostName);
            System.out.println("Adreça IP = " + ipAddr);
            System.out.println("Nom d'usuari = " + System.getProperty("user.name"));
            System.out.println("Carpeta personal = " + System.getProperty("user.home"));
            System.out.println("Sistema operatiu = " + System.getProperty("os.name"));
            System.out.println("Versió SO = " + System.getProperty("os.version"));
        }catch(UnknownHostException e){
            e.printStackTrace();
            
        }
    }
    
}
